package com.example.dipti.beacon;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.TextView;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;

import java.util.List;
import java.util.UUID;
import android.support.v7.app.AppCompatActivity;


public class BeaconActivity extends Application{

    private BeaconManager beaconManager;
    private AudioManager myAudioManager;
    @Override
    public void onCreate() {
        super.onCreate();

        beaconManager = new BeaconManager(getApplicationContext());
        beaconManager.setMonitoringListener(new BeaconManager.MonitoringListener() {
            @Override
            public void onEnteredRegion(Region region, List<Beacon> list) {
                Log.i("BeaconActivity", "I just saw a beacon for the first time!");

                myAudioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
                myAudioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);

         //       NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
         //       manager.notify(0, mBuilder.build());

                showNotification("Vibration mode Activated","Entered classroom!Phone set to vibration mode");
                MainActivity.textViewObj.setText("Sshhh!!!   You are in classroom.Setting your phone to vibration mode.");
            }
            @Override
            public void onExitedRegion(Region region) {
               Log.i("BeaconActivity", "Exiting!");
                MainActivity.textViewObj.setText("Scanning for beacons in nearby region..");
            }




        });
     //   beaconManager.setBackgroundScanPeriod(5000, 10000);

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
                                  @Override
                                  public void onServiceReady() {
                                      beaconManager.startMonitoring(new Region("my region",UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"),
                                              42869, 37330));
                                  }
                              });

    }
    public void showNotification(String title, String message) {
        Intent notifyIntent = new Intent(this, MainActivity.class);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivities(this, 0,
                new Intent[]{notifyIntent}, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);
    }

}
